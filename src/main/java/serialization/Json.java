package serialization;

import com.google.gson.Gson;

public final class Json<T> {
    private static final Gson serialization = new Gson();

    private Json() {}

    public static <T> String toJson(T data){
        return serialization.toJson(data);
    }

    public static <T> T fromJson(String jsonData, Class<T> classInstance){
        return serialization.fromJson(jsonData, classInstance);
    }
}
