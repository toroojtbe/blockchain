package hashing;

import org.apache.commons.codec.digest.DigestUtils;

public final class ApacheHashing implements Hashing{
    @Override
    public String getHash(String data){
        return getSha384Hash(data);
    }

    public final String getMd5Hash(String data){
        return DigestUtils.md5Hex(data).toString();
    }

    public final String getMd2Hash(String data){
        return DigestUtils.md2Hex(data).toString();
    }

    public final String getSha256Hash(String data){
        return DigestUtils.sha256Hex(data).toString();
    }

    public final String getSha384Hash(String data){
        return DigestUtils.sha384Hex(data).toString();
    }

    public final String getSha512Hash(String data){
        return DigestUtils.sha512Hex(data).toString();
    }
}
