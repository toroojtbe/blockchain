package hashing;

public interface Hashing {
    String getHash(String data);
}
