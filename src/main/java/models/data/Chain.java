package models.data;

import hashing.Hashing;
import serialization.Json;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class Chain<T> {
    private List<AbstractBlock> chain;
    private boolean isGenesisBlockInitialized;
    private final int DEFAULT_DIFFICULTY;
    private final char MATCHING_CHARACTER;
    private final Hashing hashing;

    public Chain(int defaultDifficulty, char matchingCharacter, Hashing hashing){
        this.DEFAULT_DIFFICULTY = defaultDifficulty;
        this.MATCHING_CHARACTER = matchingCharacter;
        this.isGenesisBlockInitialized = false;
        this.chain = new ArrayList<>();
        this.hashing = hashing;
        initializeGenesisBlock();
    }

    private boolean initializeGenesisBlock(){
        if(!isGenesisBlockInitialized){
            isGenesisBlockInitialized = true;
            chain.add(Block.newInstance("", hashing.getHash("GENESIS"), "", 0, DEFAULT_DIFFICULTY));
        }
        return isGenesisBlockInitialized;
    }

    public final boolean addBlock(T data){
        return chain.add(mineNewBlock(getLastBlock(), data));
    }

    private AbstractBlock mineNewBlock(AbstractBlock previousBlock, T data){
        String previousHash = previousBlock.getPreviousHash();
        int difficulty = previousBlock.getDifficulty();
        String hash = "";
        int nonce = 0;
        long timestamp = 0;

        do{
            timestamp = new Date().getTime();
            nonce = new Random(timestamp).nextInt();
            //difficulty += (previousBlock.getTimestamp() - timestamp) > MINING_RATE ? -1 : 1;
            hash = hashing.getHash(data.toString() + timestamp + previousHash + nonce);
        } while(!hash.startsWith(getMatch(difficulty)));

        return Block.newInstance(data, hash, previousBlock.getHash(), nonce, difficulty);
    }

    private AbstractBlock getLastBlock(){
        return chain.get(chain.size()-1);
    }

    public final boolean isValidChain(){
        if(!isGenesisBlockInitialized){
            return false;
        }
        return validateEachBlock();
    }

    private boolean validateEachBlock(){
        int chainSize = chain.size();
        for(int index=1; index<chainSize; index++){
            AbstractBlock currentBlock = chain.get(index);
            AbstractBlock previousBlock = chain.get(index-1);
            if(!currentBlock.getPreviousHash().equals(previousBlock.getHash())){
                return false;
            }
        }
        return true;
    }

    public final boolean replaceChain(Chain chain){
        List<AbstractBlock> newChain = chain.getChain();
        if(newChain.isEmpty()){
            return false;
        }
        else{
            this.chain = newChain;
            return true;
        }
    }

    public final void consolePrintChain(){
        for(AbstractBlock currentBlock: chain){
            System.out.println("Data: " + currentBlock.getData());
            System.out.println("Timestamp: " + currentBlock.getTimestamp());
            System.out.println("Hashing: " + currentBlock.getHash());
            System.out.println("Previous Hashing: " + currentBlock.getPreviousHash());
            System.out.println("Nounce: " + currentBlock.getNonce());
            System.out.println("Difficulty: " + currentBlock.getDifficulty());
            System.out.println("----------------------------------------------------");
        }
    }

    @Override
    public final String toString(){
        return Json.toJson(this.chain);
    }

    public final List getChain(){
        return chain;
    }

    private String getMatch(int difficulty){
        StringBuilder match = new StringBuilder();
        for(int index=0; index<difficulty; index++){
            match.append(MATCHING_CHARACTER);
        }

        return match.toString();
    }
}
