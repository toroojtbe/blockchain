package models.data;

abstract class AbstractBlock<T> {
    abstract long getTimestamp();

    abstract T getData();

    abstract String getPreviousHash();

    abstract int getDifficulty();

    abstract int getNonce();

    abstract String getHash();
}
