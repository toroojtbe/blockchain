package models.data;
import serialization.Json;

import java.util.Date;

class Block<T> extends AbstractBlock<T> {
    private final long timestamp;
    private final T data;
    private final String previousHash;
    private String hash;
    private int nonce;
    private int difficulty;

    private Block(T data, String hash, String previousHash, int nonce, int difficulty){
        this.data = data;
        this.previousHash = previousHash;
        this.timestamp = new Date().getTime();
        this.nonce = nonce;
        this.hash = hash;
        this.difficulty = difficulty;
    }

    static <T> AbstractBlock newInstance(T data, String hash, String previousHash, int nonce, int difficulty){
        return new Block(data, hash, previousHash, nonce, difficulty);
    }

    @Override
    long getTimestamp(){
        return timestamp;
    }

    @Override
    T getData() {
        return data;
    }

    @Override
    String getPreviousHash() {
        return previousHash;
    }

    @Override
    int getDifficulty(){
        return difficulty;
    }

    @Override
    int getNonce(){
        return nonce;
    }

    @Override
    String getHash() {
        return hash;
    }

    @Override
    public String toString(){
        return Json.toJson(this);
    }
}
