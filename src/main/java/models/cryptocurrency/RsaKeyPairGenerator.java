package models.cryptocurrency;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;


public class RsaKeyPairGenerator {
    public static final int DEFAULT_KEY_SIZE = 1024;

    private RsaKeyPairGenerator() {}

    private static KeyPairGenerator getKeyPairGeneratorInstance(){
        KeyPairGenerator keyGen;
        try{
            keyGen = KeyPairGenerator.getInstance("RSA");
            keyGen.initialize(DEFAULT_KEY_SIZE, new SecureRandom());
            return keyGen;
        }
        catch(NoSuchAlgorithmException e){
            System.out.println("Invalid KeyPairGenerator algorithm or provider instance.");
            return null;
        }
    }

    public static KeyPair generateNewKeyPair(){
        return getKeyPairGeneratorInstance().generateKeyPair();
    }
}
