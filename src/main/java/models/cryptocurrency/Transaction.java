package models.cryptocurrency;

import hashing.Hashing;

import java.security.*;
import java.util.Base64;
import java.util.Random;

public final class Transaction {
    private final PublicKey sender;
    private final PublicKey receiver;
    private final double amount;
    private final String transactionHash;
    private byte[] signature;
    private final Base64.Encoder encoder;

    public Transaction(PublicKey sender, PublicKey receiver, double amount, Hashing hashing){
        this.sender = sender;
        this.receiver = receiver;
        this.amount = amount;
        encoder = Base64.getEncoder();
        long nounce = new Random().nextLong();
        transactionHash = hashing.getHash(getKeyAsString(sender) + getKeyAsString(receiver) + amount + nounce);
    }

    public void generateSignature(PrivateKey privateKey){
        signature = signWithRsaSha256(privateKey, transactionHash);
    }

    public boolean validateSignature(){
        return validateRsaSha256Sign(sender, transactionHash, signature);
    }

    private String getKeyAsString(Key key){
        return encoder.encodeToString(key.getEncoded());
    }

    public static byte[] signWithRsaSha256(PrivateKey privateKey, String data){
        Signature signer;
        byte[] signature;
        try{
            signer = Signature.getInstance("SHA256withRSA");
            signer.initSign(privateKey);
            byte[] dataInBytes = data.getBytes();
            signer.update(dataInBytes);
            signature = signer.sign();
        }
        catch(NoSuchAlgorithmException e){
            System.out.println("Invalid instance of algorithm.");
            throw new RuntimeException(e);
        }
        catch(InvalidKeyException e){
            System.out.println("Invalid private key.");
            throw new RuntimeException(e);
        }
        catch(SignatureException e){
            System.out.println("Invalid signature found.");
            throw new RuntimeException(e);
        }

        return signature;
    }

    public static boolean validateRsaSha256Sign(PublicKey publickey, String data, byte[] signature){
        try{
            Signature verifier = Signature.getInstance("SHA256withRSA");
            verifier.initVerify(publickey);
            verifier.update(data.getBytes());
            return verifier.verify(signature);
        }
        catch(NoSuchAlgorithmException e){
            System.out.println("Invalid instance of algorithm.");
            throw new RuntimeException(e);
        }
        catch(InvalidKeyException e){
            System.out.println("Invalid private key.");
            throw new RuntimeException(e);
        }
        catch(SignatureException e){
            System.out.println("Invalid signature found.");
            throw new RuntimeException(e);
        }
    }
}
