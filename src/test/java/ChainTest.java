import static org.junit.Assert.*;

import hashing.ApacheHashing;
import models.data.Chain;
import org.junit.Test;

public class ChainTest {
    @Test
    public void validateChain(){
        Chain chain = new Chain(1, '0', new ApacheHashing());
        assertTrue(chain.addBlock(1));
        assertTrue(chain.addBlock("a"));
        assertTrue(chain.addBlock('1'));
        assertTrue(chain.addBlock(2.0));
        assertTrue(chain.addBlock(-21));
        assertTrue(chain.isValidChain());
        chain.replaceChain(new Chain(1, 'a', new ApacheHashing()));
        assertTrue(chain.isValidChain());
    }
}
